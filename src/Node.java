
import java.util.*;
import java.util.StringTokenizer;

/** kasutatud allikad
 * https://stackoverflow.com/questions/12752225/how-do-i-find-the-position-of-matching-parentheses-or-braces-in-a-given-piece-of
 * https://enos.itcollege.ee/~jpoial/algoritmid/puud.html
 */

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      setName (n);
      setFirstChild (d);
      setRightSibling (r);
   }

   Node (String s) {
      this (s, null, null);
   }

   Node (Node s) {
      this (s.name, s.firstChild, s.nextSibling);
   }

   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setRightSibling (Node p) {
      nextSibling = p;
   }

   public Node getRightSibling() {
      return nextSibling;
   }

   public void setFirstChild (Node a) {
      firstChild = a;
   }

   public Node getFirstChild() {
      return firstChild;
   }
   @Override
   public String toString() {
      return leftParentheticRepresentation();
   }
   

   public boolean hasNext() {
      return (getRightSibling() != null);
   }

   public Node next() {
      return getRightSibling();
   }

   public Node children() {
      return getFirstChild();
   }

   public void addChild (Node a) {
      if (a == null) return;
      Node children = children();
      if (children == null)
         setFirstChild (a);
      else {
         while (children.hasNext())
            children = children.next();
         children.setRightSibling (a);
      }
   }


   public static Node parsePostfix (String s) {
      while (s.contains(", ")){
         s = s.replace(", ", ",");
      }
      while (s.contains("( ")){
         s = s.replace("( ", "(");
      }
      while (s.contains(",\t")){
         s = s.replace(",\t", ",");
      }
      while (s.contains("(\t")){
         s = s.replace("(\t", "(");
      }
      if (s.contains("\t")){
         throw new RuntimeException("Node name can't contain tabs in: " + s);
      }
      if (s.contains(" ")){
         throw new RuntimeException("Node name can't contain empty space in: " + s);
      }



      StringBuilder f = new StringBuilder();
      for (int i = 0; i < s.length(); i++) {
         if (s.charAt(i) != ',' && s.charAt(i) != '(' && s.charAt(i) != ')' && s.charAt(i) != ' '){
            f.append(s.charAt(i));
         }
      }
      if (f.length() == 0){
         throw new RuntimeException("Input string must contain at least 1 node in: " + s);
      }
      if (!s.contains("(") && !s.contains("(") && s.contains(",")){
         throw new RuntimeException("Input can't contain more than 1 tree in: " + s);
      }

      StringTokenizer str = new StringTokenizer(s, "()," ,true);
      StringTokenizer illegalArgumentFinder = new StringTokenizer(s, "(),", true );
      String curOp = "";
      String nextOp = "";
      String illCur = "";
      String illNext = "";
      Boolean wasComa = false;
      while (illegalArgumentFinder.hasMoreTokens()){
         illCur = illegalArgumentFinder.nextToken();

         if (illegalArgumentFinder.hasMoreTokens()){
            illNext = illegalArgumentFinder.nextToken();
         }else{
            break;
         }
         if (illCur.equals("(") && illNext.equals(",")){
            throw new RuntimeException("'(' cant be followed by ',' in:" + s);
         }
         if (illCur.equals("(") && illNext.equals(")")){
            throw new RuntimeException("Subtree can't be empty in: " + s);
         }
      }

      StringBuilder subS = new StringBuilder();

      List<Node> list = new ArrayList<>();
      Node root = new Node(null , null, null);
      Node curNode = new Node(null, null, null);

      while (str.hasMoreTokens()){

         curOp = str.nextToken();
         if (curOp.equals(",")){
            throw new RuntimeException("Input can't contain more than 1 tree in: " + s);
         }

         subS.append(curOp);
         if (!str.hasMoreTokens()){
            root.name = curOp;
         }
         if (curOp.equals(")")){
            curOp = str.nextToken();
            root.name = curOp;
            subS.append(curOp);
            for (Node e: list) {
               if (!e.name.equals(",")){
                  root.addChild(e);
               }
            }
         }

         if (curOp.equals("(")){

            while (!nextOp.equals(")")){

               if (!nextOp.equals("(")){
                  nextOp = str.nextToken();
                  subS.append(nextOp);
                  if (nextOp.equals(",") && wasComa){
                     throw new RuntimeException("',' can't be followed by another ',' in: " + s);
                  }
               }

               if (nextOp.equals("(")){

                  String t = s.substring(subS.length() - 1);

                  int closePos = 0;
                  int counter = 1;
                  StringTokenizer str2 = new StringTokenizer(s.substring(subS.length()), "(),", true);
                  String c = "";
                  while (counter > 0) {
                     if (str2.hasMoreTokens()){

                        c = str2.nextToken();
                     }

                     if (c.equals("(")) {
                        counter++;
                     }
                     else if (c.equals(")")) {
                        counter--;
                     }
                     closePos += c.length();
                  }


                  Node tmp = new Node(parsePostfix(t.substring(0, closePos + 2)));
                  list.add(tmp);
                  StringTokenizer str3 = new StringTokenizer(tmp.toString(), "(),", true);
                  String go = "";
                  int passedTokens = 0;
                  while (str3.hasMoreTokens()){
                     go = str3.nextToken();
                     passedTokens++;
                  }

                  for (int i = 0; i < passedTokens; i++) {
                     nextOp = str.nextToken();

                     subS.append(nextOp);
                  }

               }else if (!nextOp.equals(",") && !nextOp.equals(")")){
                  list.add(new Node(nextOp));

               }
               if(nextOp.equals(",")){
                  wasComa = true;
               }else {
                  wasComa = false;
               }

            }
            curOp = str.nextToken();
            subS.append(curOp);
            curNode.name = curOp;
            for (Node e: list) {
               if (e.name != null){
                  if (!e.name.equals(",")){
                     curNode.addChild(e);
                  }
               }

            }

            if (root.name != null){
               root.addChild(new Node(curNode));

            }else{
               list = new ArrayList<>();
               list.add(new Node(curNode));
               if (!str.hasMoreTokens()){
                  root = curNode;
               }

            }
         }
      }

      return root;
   }

   public String leftParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      b.append(getName());
      if (getFirstChild() != null){

         b.append("(");
         b.append(getFirstChild());
         Node first = getFirstChild();
         while (first.hasNext()){
            b.append(",");
            b.append(first.next());
            first = first.next();

         }
         b.append(")");
      }

      return b.toString();
   }




   public String toXML(int depth) {
      if (depth == 0){
         depth = 1;
      }
      StringBuffer b = new StringBuffer();
      b.append("<L" + depth + ">");
      b.append(" " + getName() + " ");
      if (getFirstChild() != null){
         Node subTree = this.getFirstChild();
         String newRes = subTree.toXML(depth + 1);
         b.append("\n");
         for (int i = 0; i < depth; i++) {
            b.append("\t");
         }
         b.append(newRes);
         b.append("\n");
         for (int i = 0; i < depth - 1; i++) {
            b.append("\t");
         }
         b.append("</L" + depth + ">");


      }
      if (this.hasNext()){

         Node nextTree = this.nextSibling;
         String newRes = nextTree.toXML(depth);
         String check = String.valueOf(b.charAt(b.length() - 1));
         if (!check.equals(">")){
            b.append("</L" + depth + ">");
         }

         b.append("\n");
         for (int i = 0; i < depth - 1; i++) {
            b.append("\t");
         }
         b.append(newRes);


      }
      String check = String.valueOf(b.charAt(b.length() - 1));
      if (this.getFirstChild() == null && !this.hasNext() && !check.equals(">")){
         b.append("</L" + depth + ">");
      }




      return b.toString();
   }


   public static void main (String[] param) {
//      String s = "((X,M)B,(F)D,(U,Z,O)G)A";
//      Node t = Node.parsePostfix (s);
//      String v = t.leftParentheticRepresentation();
//      String x = t.pseudoXMLRepresentation();
//      System.out.println(v);
//      System.out.println(x);
      String n = "(((2,1)-,4)*,(69,3)/)+";
      Node nod = Node.parsePostfix(n);
      String res = nod.toXML(0);
      System.out.println(res);

      //System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}
